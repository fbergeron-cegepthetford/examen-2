package data.quintette;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Historique.class}, version = 1)
public abstract class QuintetteBD extends RoomDatabase{

    public abstract HistoriqueDao historiqueDao();

    private static volatile QuintetteBD INSTANCE;

    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    /**
     * Patron singleton, pour n'avoir qu'une seule instance de la BD dans l'application, comme c'est
     * un objet lourd. allowMainThreadQueries n'est pas recommandé, nous verrons comment l'enlever
     * lors du cours sur le multithread en Android.
     * @param context
     * @return
     */
    static QuintetteBD getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (QuintetteBD.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            QuintetteBD.class, "tictactoeBD")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}

