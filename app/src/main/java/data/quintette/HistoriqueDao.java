package data.quintette;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface HistoriqueDao {

    @Query("SELECT * FROM historique")
    List<Historique> toutRecuperer();

    @Query("SELECT * FROM historique WHERE uid=:joueursIds")
    Historique recupererParId(int joueursIds);

    @Query("SELECT * FROM historique WHERE mot LIKE :nom")
    List<Historique> trouverParMot(String nom);

    @Insert
    void insererPlusieurs(Historique... joueurs);

    @Delete
    void effacer(Historique joueurs);

    @Update
    int modifier(Historique joueur);

}