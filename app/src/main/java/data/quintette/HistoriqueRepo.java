package data.quintette;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

public class HistoriqueRepo {
    private HistoriqueDao historiqueDAO;

    public HistoriqueRepo(Application application) {
        QuintetteBD db = QuintetteBD.getDatabase(application);
        historiqueDAO = db.historiqueDao();

    }

    public List<Historique> listeJoueurs() {
        return historiqueDAO.toutRecuperer();
    }

    public void ajouter(Historique nouveau) {
        QuintetteBD.databaseWriteExecutor.execute(() -> {
            historiqueDAO.insererPlusieurs(nouveau);
        });
    }

    public Historique recupererParId(int id){
        //TODO
        return new Historique("");
    }

    public List<Historique> recupererMot(String nom){
        List<Historique> tuples = new ArrayList<>();
        //TODO
        return tuples;
    }

    public void inserer(Historique joueur){
        //TODO
    }

    public void modifier(Historique joueur){
        //TODO
    }

    public void effacer(Historique joueur){
        //TODO
    }
}
