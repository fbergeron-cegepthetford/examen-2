package data.quintette;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Historique {

    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name= "mot")
    public String mot;

    @ColumnInfo (name = "victoire")
    public boolean victoire;

    @ColumnInfo (name = "temps")
    public String temps;

    Historique(String mot){
        this.mot = mot;
        this.victoire = false;
        this.temps = "";
    }
}