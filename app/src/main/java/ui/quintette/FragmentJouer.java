package ui.quintette;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import affaire.quintette.Quintette;
import affaire.quintette.Validation;
import ui.quintette.databinding.FragmentJouerBinding;

public class FragmentJouer extends Fragment {



    private FragmentJouerBinding binding;
    private Context cx;
    private Quintette quintette;
    private List<List<TextView>> cases;

    private Instant debut;


    public FragmentJouer() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentJouerBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        this.cx = getActivity().getApplicationContext();

        debut = Instant.now();
        quintette = new Quintette();
        initialiserVue();
        initialiserListeners();
    }

    private void initialiserVue() {
        cases = new ArrayList<>();
        List<TextView> tour1 = new ArrayList<>();
        List<TextView> tour2 = new ArrayList<>();
        List<TextView> tour3 = new ArrayList<>();
        List<TextView> tour4 = new ArrayList<>();
        List<TextView> tour5 = new ArrayList<>();
        List<TextView> tour6 = new ArrayList<>();

        tour1.add(binding.tvCase11);
        tour1.add(binding.tvCase12);
        tour1.add(binding.tvCase13);
        tour1.add(binding.tvCase14);
        tour1.add(binding.tvCase15);
        cases.add(tour1);

        tour2.add(binding.tvCase21);
        tour2.add(binding.tvCase22);
        tour2.add(binding.tvCase23);
        tour2.add(binding.tvCase24);
        tour2.add(binding.tvCase25);
        cases.add(tour2);

        tour3.add(binding.tvCase31);
        tour3.add(binding.tvCase32);
        tour3.add(binding.tvCase33);
        tour3.add(binding.tvCase34);
        tour3.add(binding.tvCase35);
        cases.add(tour3);

        tour4.add(binding.tvCase41);
        tour4.add(binding.tvCase42);
        tour4.add(binding.tvCase43);
        tour4.add(binding.tvCase44);
        tour4.add(binding.tvCase45);
        cases.add(tour4);

        tour5.add(binding.tvCase51);
        tour5.add(binding.tvCase52);
        tour5.add(binding.tvCase53);
        tour5.add(binding.tvCase54);
        tour5.add(binding.tvCase55);
        cases.add(tour5);

        tour6.add(binding.tvCase61);
        tour6.add(binding.tvCase62);
        tour6.add(binding.tvCase63);
        tour6.add(binding.tvCase64);
        tour6.add(binding.tvCase65);
        cases.add(tour6);
    }

    private void initialiserListeners() {
        binding.btnSoumettre.setOnClickListener(view ->{
            String tentative = binding.etMot.getText().toString();
            try {
                Validation[] resultat = quintette.valider(tentative);
                char[] mot = tentative.toCharArray();
                for(int index = 0; index < 5; index++){
                    TextView vue = cases.get(quintette.getTour()).get(index);
                    vue.setText(String.valueOf(mot[index]));
                    colorier(vue, resultat[index]);
                }

                gererVictoire();
                gererDefaite();


            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(cx, "Nombre d'essaie maximal atteint", Toast.LENGTH_SHORT).show();
            }

            binding.etMot.setText("");
        });
    }

    private void sauvegarder() {
        Instant fin = Instant.now();
        Duration durationPartie = Duration.between(debut, fin);
        String tempsPartie = durationPartie.toString(); //Voici comment obtenir le temps de la partie.
        //TODO sauvegarder dans la bd
    }

    private void gererDefaite() {
        //TODO, parce que le code actuel ne permet pas de le faire
    }

    private void colorier(TextView vue, Validation validation) {
        switch (validation){
            case ABSENT:
                vue.setBackground(ContextCompat.getDrawable(cx, R.drawable.bg_gris));
                break;
            case PRESENT:
                vue.setBackground(ContextCompat.getDrawable(cx, R.drawable.bg_existe));
                break;
            case VALIDE:
                vue.setBackground(ContextCompat.getDrawable(cx, R.drawable.bg_bon));
                break;
        }
    }

    private void gererVictoire(){
        if(quintette.getVictoire()){
            binding.btnSoumettre.setEnabled(false);
            sauvegarder();
        }
    }

    //@Override
    public void update(Observable observable, Object o) {
        if(observable == quintette){
            Pair<String, Object> message = (Pair<String, Object>) o;
            if(message.first.equals("victoire")){
                //TODO
            }
            else if(message.first.equals("validation")){
                Validation[] resultat = (Validation[]) message.second;
                //TODO
            }
            else if(message.first.equals("defaite")){
                //TODO
            }
        }
    }


}