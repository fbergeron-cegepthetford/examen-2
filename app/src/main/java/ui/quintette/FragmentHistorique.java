package ui.quintette;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import data.quintette.Historique;
import data.quintette.HistoriqueRepo;

public class FragmentHistorique extends Fragment {
    HistoriqueRepo hr;

    ListView lvHistorique;
    Context cx;

    public FragmentHistorique() {
        // Constructeur vide requis
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_historique, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        cx = getActivity().getApplicationContext();
        initialiserModele();
        initialiserVue(view);
    }

    private void initialiserVue(View view) {
        //TODO
    }

    private void initialiserModele() {
        hr = new HistoriqueRepo(requireActivity().getApplication());
    }

    public class HistoriqueAdapter extends ArrayAdapter<Historique> {

        public HistoriqueAdapter(Context context, ArrayList<Historique> historiques) {
            super(context, 0, historiques);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Recuperer le tuple courant
            Historique historique = getItem(position);

            // Valider si on a deja une vue en main
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.historique_listview, parent, false);
            }

            //TODO

            return convertView;
        }

    }
}