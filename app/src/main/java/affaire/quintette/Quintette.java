package affaire.quintette;

import java.util.Map;
import java.util.Observable;

public class Quintette extends Observable {

    private String mot;
    private final int MAX_TOURS = 6;
    private int tour = 0;
    private boolean victoire = false;

    public int getTour() {
        return tour;
    }



    public Quintette(){
        this.mot = Mots.motDuJour();
    }

    public Quintette(String mot){
        this.mot = mot;
    }

    public Validation[] valider(String motAValider) throws Exception {
        if(tour >= MAX_TOURS)
            throw new Exception("Tour maximal atteint");

        Validation[] retour = new Validation[5];
        for (int index = 0; index < 5; index++){
            if(mot.charAt(index) == motAValider.charAt(index)){
                //Cas simple, c'est valide
                retour[index] = Validation.VALIDE;
            }
            else if(mot.contains(String.valueOf(motAValider.charAt(index)))){
                retour[index] = Validation.PRESENT;
            }
            else{
                retour[index] = Validation.ABSENT;
            }
        }

        if(motAValider.equals(mot))
            this.victoire = true;

        this.tour++;

        return retour;
    }

    public boolean getVictoire(){
        return this.victoire;
    }
}
