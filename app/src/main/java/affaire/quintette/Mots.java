package affaire.quintette;

import java.util.List;
import java.util.Random;

public class Mots {

    public static String motDuJour(){
        List<String> givenList = java.util.Arrays.asList("table", "vingt", "mange", "oncle", "pieds", "assez", "autre", "pouet");
        Random rand = new Random(System.currentTimeMillis());
        return givenList.get(rand.nextInt(givenList.size()));
    }
}
